"""Start file of app"""
# !/usr/bin/env python
from aiogram.types import AllowedUpdates
from aiogram.utils.executor import start_polling

import middlewares
from handlers import dp
from utils.logging import setup as logging_setup


async def on_startup(dispatcher):
    middlewares.setup(dispatcher)

if __name__ == '__main__':
    logging_setup()
    start_polling(dispatcher=dp,
                  on_startup=on_startup,
                  allowed_updates=AllowedUpdates.all(),
                  skip_updates=True)
