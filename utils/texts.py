"""Texts used in bot"""
FIATUM_TEXT = 'OUR PRODUCTS'
FIATUM_LINK = ('https://fiatum.com/?utm_source=telegram'
               '&utm_medium=news&utm_campaign=community')
CHAT_TEXT = 'REQUEST A QUOTE'
CHAT_LINK = 'https://t.me/fiatum_chat'
NEW_MEMBER_TEXT = ('Hello, {}!\n\n'
                   'WELCOME TO THE OFFICIAL FIATUM ENGLISH CHAT!\n\n'
                   'This place is for discussing '
                   f'<a href="{FIATUM_LINK}">FIATUM</a> '
                   'project: news, updates and closely related themes \n\n'
                   '<a href="https://t.me/fiatum_chat/4">CHAT RULES</a>\n'
                   '<a href="https://t.me/fiatum_chat/5">IMPORTANT INFO</a>')
NEW_MEMBER_PHOTO_ID = ('AgACAgIAAxkBAAMNYROQebTZ0PQn2GWMZO'
                       'nkj2SBrAsAAqu3MRuc0JlI_P8f1i5cH'
                       'vgBAAMCAAN5AAMgBA')

START_BOT_TEXT = 'Hello, {}!'
SUITS_YOU_TEXT = 'What product suits you best?'
CONTACT_SOON_TEXT = 'Our manager will contact you soon.'
HAVE_LUCK_TEXT = 'Have luck!'
MANAGER_CHAT_ID = 766130982
