"""Register all dispatcher handlers"""
from .base_handler import dp

__all__ = ['dp']
