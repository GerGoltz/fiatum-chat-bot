"""States"""
from aiogram.dispatcher.filters.state import StatesGroup, State


class Private(StatesGroup):
    """States for PM"""
    First = State()
    Last = State()
