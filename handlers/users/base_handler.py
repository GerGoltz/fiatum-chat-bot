"""Reaction on /start command"""
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import (Message, ChatType,
                           ReplyKeyboardMarkup, KeyboardButton,
                           ReplyKeyboardRemove)
from loguru import logger

from handlers.users.states import Private
from utils.loader import dp, bot
from utils.texts import (START_BOT_TEXT, SUITS_YOU_TEXT,
                         CONTACT_SOON_TEXT, MANAGER_CHAT_ID,
                         HAVE_LUCK_TEXT)

cancel_markup = ReplyKeyboardMarkup(keyboard=[[KeyboardButton('Cancel')]],
                                    resize_keyboard=True)
remove_markup = ReplyKeyboardRemove()


@dp.message_handler(Text(equals='Cancel'), state='*',
                    chat_type=ChatType.PRIVATE)
async def cancel(message: Message, state: FSMContext):
    """Command cancel handling"""
    logger.info(f'{message.text} by {message.from_user.full_name}. '
                f'USER_ID: {message.from_user.id}')
    await message.answer(HAVE_LUCK_TEXT, reply_markup=remove_markup)
    await state.finish()


@dp.message_handler(chat_type=ChatType.PRIVATE)
async def bot_start(message: Message):
    """Command private messages handling"""
    logger.info(f'{message.text} by {message.from_user.full_name}. '
                f'USER_ID: {message.from_user.id}')
    await message.answer(START_BOT_TEXT.format(message.from_user.full_name),
                         reply_markup=cancel_markup)
    await Private.first()


@dp.message_handler(state=Private.First,
                    chat_type=ChatType.PRIVATE)
async def first(message: Message, state: FSMContext):
    """State first handling"""
    logger.info(f'{message.text} by {message.from_user.full_name}. '
                f'USER_ID: {message.from_user.id}')
    await state.set_data({'message_id': message.message_id,
                          'from_chat_id': message.chat.id})
    await message.answer(SUITS_YOU_TEXT)
    await Private.next()


@dp.message_handler(state=Private.Last,
                    chat_type=ChatType.PRIVATE)
async def last(message: Message, state: FSMContext):
    """State last handling"""
    logger.info(f'{message.text} by {message.from_user.full_name}. '
                f'USER_ID: {message.from_user.id}')
    await message.answer(CONTACT_SOON_TEXT, reply_markup=remove_markup)
    previous_message = await state.get_data()
    await state.finish()
    try:
        for message in (previous_message, {'message_id': message.message_id,
                                           'from_chat_id': message.chat.id}):
            await bot.forward_message(chat_id=MANAGER_CHAT_ID,
                                      **message)
    except Exception as error:
        logger.warning(f'{error.__class__.__name__}: {error}')
