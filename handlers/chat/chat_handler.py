"""Chat handler"""
import asyncio
import json
from contextlib import suppress

from aiogram.types import (ChatMemberUpdated, Message,
                           InlineKeyboardMarkup, InlineKeyboardButton)
from aiogram.utils.exceptions import (MessageCantBeDeleted,
                                      MessageToDeleteNotFound)
from loguru import logger

from utils.loader import dp, bot
from utils.texts import (NEW_MEMBER_TEXT, NEW_MEMBER_PHOTO_ID,
                         FIATUM_LINK, CHAT_LINK,
                         FIATUM_TEXT, CHAT_TEXT)

REPLY_MARKUP = InlineKeyboardMarkup(inline_keyboard=[[
    InlineKeyboardButton(text=FIATUM_TEXT, url=FIATUM_LINK),
    InlineKeyboardButton(text=CHAT_TEXT, url=CHAT_LINK)
]])


def filter_new(member: ChatMemberUpdated):
    """Filter only new members of chat"""
    return member.new_chat_member.status == 'member'


async def delete_message(message: Message, sleep_time: int = 0):
    """Delete hello message task"""
    await asyncio.sleep(sleep_time)
    with suppress(MessageCantBeDeleted, MessageToDeleteNotFound):
        await message.delete()


@dp.chat_member_handler(filter_new)
async def new_member(member: ChatMemberUpdated):
    """new member greeting"""
    messages = json.loads(await dp.storage._redis.get('message') or '{}')
    for chat_id, message_id in messages:
        with suppress(MessageCantBeDeleted, MessageToDeleteNotFound):
            await bot.delete_message(message_id=message_id,
                                     chat_id=chat_id)
    user = member.new_chat_member.user
    logger.info(f'new member: {user.full_name}')
    user_verbose = f'<a href="{user.url}">{user.full_name}</a>'
    message = await bot.send_photo(
        photo=NEW_MEMBER_PHOTO_ID,
        chat_id=member.chat.id,
        caption=NEW_MEMBER_TEXT.format(user_verbose),
        disable_notification=True,
        reply_markup=REPLY_MARKUP
    )
    asyncio.create_task(delete_message(message, 60))
    await dp.storage._redis.set('message',
                                json.dumps(((message.chat.id,
                                             message.message_id),)),
                                expire=60)


@dp.message_handler(content_types=['new_chat_members', 'left_chat_member'])
async def delete_service_msgs(message: Message):
    """delete service messages"""
    with suppress(MessageCantBeDeleted, MessageToDeleteNotFound):
        await message.delete()
