FROM python:3.9-slim-buster

WORKDIR /app
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y gcc

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/localtime

COPY . ./

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
